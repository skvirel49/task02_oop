package com.bilas.model;

public abstract class House {
    private String address;
    private int price;
    private int distanceToSchool;
    private int distanceToKindergarten;

    public House(String address, int price, int distanceToSchool, int distanceToKindergarten) {
        this.address = address;
        this.price = price;
        this.distanceToSchool = distanceToSchool;
        this.distanceToKindergarten = distanceToKindergarten;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDistanceToSchool() {
        return distanceToSchool;
    }

    public void setDistanceToSchool(int distanceToSchool) {
        this.distanceToSchool = distanceToSchool;
    }

    public int getDistanceToKindergarten() {
        return distanceToKindergarten;
    }

    public void setDistanceToKindergarten(int distanceToKindergarten) {
        this.distanceToKindergarten = distanceToKindergarten;
    }

    @Override
    public String toString() {
        return "House{" +
                "address='" + address + '\'' +
                ", price=" + price +
                ", distanceToSchool=" + distanceToSchool +
                ", distanceToKindergarten=" + distanceToKindergarten +
                '}';
    }
}
