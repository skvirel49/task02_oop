package com.bilas.model;

public class Cottage extends House {
    private boolean garage;

    public Cottage(String address, int price, int distanceToSchool, int distanceToKindergarten, boolean garage) {
        super(address, price, distanceToSchool, distanceToKindergarten);
        this.garage = garage;
    }

    public boolean isGarage() {
        return garage;
    }

    public void setGarage(boolean garage) {
        this.garage = garage;
    }

    @Override
    public String
    toString() {
        return "Cottage{" +
                " address=" +
                getAddress() +
                ", price=" +
                getPrice() +
                ", garage=" + garage +
                '}';
    }
}
