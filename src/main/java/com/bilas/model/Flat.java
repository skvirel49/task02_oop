package com.bilas.model;

public class Flat extends House {
    private boolean playground;
    private int floor;

    public Flat(String address, int price, int distanceToSchool, int distanceToKindergarten, boolean playground, int floor) {
        super(address, price, distanceToSchool, distanceToKindergarten);
        this.playground = playground;
        this.floor = floor;
    }

    public boolean isPlayground() {
        return playground;
    }

    public void setPlayground(boolean playground) {
        this.playground = playground;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "Flat{" +
                " address=" +
                getAddress() +
                ", price=" +
                getPrice() +
                ", playground=" + playground +
                ", floor=" + floor +
                '}';
    }
}
