package com.bilas.model;

public class Penthouse extends House{
    public Penthouse(String address, int price, int distanceToSchool, int distanceToKindergarten) {
        super(address, price, distanceToSchool, distanceToKindergarten);
    }
}
