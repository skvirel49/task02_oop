package com.bilas.model;

import java.util.List;

public class BusinessLogic implements Model{

    private Market market;

    public BusinessLogic() {
        this.market = new Market();
    }


    public List<House> getHouseList() {
        return market.getHouseList();
    }

    public List<House> getAllHouses() {
        return market.getHouseList();
    }

    public List<House> getHouseCheaperThen(int price) {
        market.getHouseCheaperThen(price);
        return market.getHouseList();
    }

    public List<House> getHouseByAddress(String address) {
        market.getHouseByAddress(address);
        return market.getHouseList();
    }

    public List<House> getHouseByDistanceFromSchool(int distance) {
        market.getHouseByDistanceFromSchool(distance);
        return market.getHouseList();
    }
}
