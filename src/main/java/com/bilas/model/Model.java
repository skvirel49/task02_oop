package com.bilas.model;

import java.util.List;

public interface Model {

    List<House> getHouseList();
    List<House> getAllHouses();
    List<House> getHouseCheaperThen(int price);
    List<House> getHouseByAddress(String address);
    List<House> getHouseByDistanceFromSchool(int distance);

}
