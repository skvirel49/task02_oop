package com.bilas.model;

import java.util.ArrayList;
import java.util.List;

public class Market {

    private List<House> houseList;

    public Market() {
        generateHouses();
    }

    private void generateHouses(){
        houseList = new ArrayList<House>();

        houseList.add(new Flat("Franka 12", 250, 600, 100, true, 4));
        houseList.add(new Flat("Vilna 1", 250, 900, 1500, false, 4));
        houseList.add(new Flat("Franka 12", 400, 600, 100, true, 5));
        houseList.add(new Flat("Zhovta 2", 250, 1300, 800, true, 9));
        houseList.add(new Flat("Stryjska 222", 300, 600, 800, true, 7));
        houseList.add(new Cottage("Zelena 50", 700, 500, 1500, true));
        houseList.add(new Cottage("Nova 300", 900, 500, 1500, true));
        houseList.add(new Cottage("Shevchenka 343", 300, 500, 800, false));
        houseList.add(new Cottage("Shevchenka 345", 250, 500, 800, false));
        houseList.add(new Cottage("Zelena 300", 700, 500, 1500, true));
        houseList.add(new Penthouse("Franka 12", 500, 600, 100));
        houseList.add(new Penthouse("Franka 12", 500, 600, 100));
        houseList.add(new Penthouse("Vilna 1", 800, 900, 1500));
        houseList.add(new Penthouse("Stryjska 222", 500, 600, 800));
        houseList.add(new Penthouse("Franka 12", 500, 600, 100));

    }

    public List<House> getHouseList() {
        return houseList;
    }

    public final List<House> getHouseCheaperThen(final int price) {
        List<House> cheaperItemList = new ArrayList<House>();
        for (House it : houseList) {
            if (it.getPrice() < price) {
                cheaperItemList.add(it);
            }
        }
        houseList = cheaperItemList;
        return houseList;
    }

    public final List<House> getHouseByAddress(final String address) {
        generateHouses();
        List<House> houses = new ArrayList<House>();
        for (House house : houseList) {
            if (house.getAddress().toUpperCase().contains(address.toUpperCase())) {
                houses.add(house);
            }
        }
        houseList = houses;
        return houseList;
    }

    public final List<House> getHouseByDistanceFromSchool(final int distance){
        generateHouses();
        List<House> houses = new ArrayList<House>();
        for (House house : houseList) {
            if (house.getDistanceToSchool() <= distance) {
                houses.add(house);
            }
        }
        houseList = houses;
        return houseList;
    }
}
