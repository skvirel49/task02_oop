package com.bilas.view;

import com.bilas.controller.Controller;
import com.bilas.controller.MyController;
import com.bilas.model.House;

import java.util.Scanner;

public class MyView implements Printable {

    private Controller controller;
    private Scanner scanner = new Scanner(System.in);
    public void print() {
        controller = new MyController();
        while (true){
            outputMenu();
            System.out.println("please select menu button:");
            int button = scanner.nextInt();
            switcher(button);
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        System.out.println("1 - Print all Houses");
        System.out.println("2 - Print Houses Cheaper then price");
        System.out.println("3 - Print Houses on Street");
        System.out.println("4 - Print Houses in distance from school");
        System.out.println("0 - to exit");
    }
    private void switcher(int num){
        switch (num){
            case 1:
                pressButton1();
                break;
            case 2:
                pressButton2();
                break;
            case 3:
                pressButton3();
                break;
            case 4:
                pressButton4();
                break;
            case 0:
                System.exit(0);
                break;
            default:
                System.out.println("Wrong key!");
                break;
        }
    }

    private void pressButton1(){
        for (House house : controller.getAllHouses()) {
            System.out.println(house);
        }
    }

    private void pressButton2(){
        System.out.println("add price:");
        for (House house : controller.getHouseCheaperThen(scanner.nextInt())
             ) {
            System.out.println(house);
        }
    }

    private void pressButton3(){
        System.out.println("add address:");
        String address = scanner.nextLine();
        for (House house : controller.getHouseByAddress(address)) {
            System.out.println(house);
        }
    }

    private void pressButton4(){
        System.out.println("add distance:");
        for (House house : controller.getHouseByDistanceFromSchool(scanner.nextInt())) {
            System.out.println(house);
        }
    }
}
