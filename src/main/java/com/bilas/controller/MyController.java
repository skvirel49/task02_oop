package com.bilas.controller;

import com.bilas.model.BusinessLogic;
import com.bilas.model.House;
import com.bilas.model.Model;

import java.util.List;

public class MyController implements Controller {

    private Model model;

    public MyController() {
        this.model = new BusinessLogic();
    }

    public List<House> getHouseList() {
        return model.getHouseList();
    }

    public List<House> getAllHouses() {
        return model.getAllHouses();
    }

    public List<House> getHouseCheaperThen(int price) {
        return model.getHouseCheaperThen(price);
    }

    public List<House> getHouseByAddress(String address) {
        return model.getHouseByAddress(address);
    }

    public List<House> getHouseByDistanceFromSchool(int distance) {
        return model.getHouseByDistanceFromSchool(distance);
    }
}
