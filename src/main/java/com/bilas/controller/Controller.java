package com.bilas.controller;

import com.bilas.model.House;

import java.util.List;

public interface Controller {

    List<House> getAllHouses();
    List<House> getHouseCheaperThen(int price);
    List<House> getHouseByAddress(String address);
    List<House> getHouseByDistanceFromSchool(int distance);

}
